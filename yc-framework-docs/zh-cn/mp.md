# MyBatis-Plus

官方网站:
https://baomidou.com/

使用文档:
https://baomidou.com/pages/24112f/

源代码:
https://github.com/baomidou/mybatis-plus


微服务中使用MyBatis-Plus，只需引入如下依赖即可:
```
<dependency>
    <groupId>com.yc.framework</groupId>
    <artifactId>yc-common-mp</artifactId>
</dependency>


```

源代码示例:
https://github.com/developers-youcong/yc-framework/tree/main/yc-example/yc-example-mp

# YC-Framework如何使用MP
[从零开始学YC-Framework之MP](https://youcongtech.com/2022/08/21/%E4%BB%8E%E9%9B%B6%E5%BC%80%E5%A7%8B%E5%AD%A6YC-Framework%E4%B9%8BMP/)